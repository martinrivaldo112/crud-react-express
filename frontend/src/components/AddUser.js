import React,{useState} from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom';

export const AddUser = () => {

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("Male");
  const navigate = useNavigate();

  const saveUser = async(events) => {
    events.preventDefault();
    try {
      await axios.post('http://localhost:5000/users', {
        name,
        email,
        gender,
      });
      navigate('/')
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className='columns mt-5 is-centered'>
      <div className='column is-half'>
            <form onSubmit={saveUser}>
                <div>
                  <label className='label'>Nama</label>
                  <div className='control'>
                    <input 
                      type='text'
                      className='input'
                      placeholder='Nama'
                      value={name}
                      onChange={(events)=> setName(events.target.value)}
                    />
                  </div>
                </div>  
                <div>
                  <label className='label'>Email</label>
                  <div className='control'>
                    <input
                      type='text'
                      className='input'
                      placeholder='Email'
                      value={email}
                      onChange={(events)=> setEmail(events.target.value)}
                    />
                  </div>
                </div>  
                <div>
                  <label className='label'>Gender</label>
                  <div className='control'>
                    <div className='select is-fullwidth'>
                      <select value={gender}
                              onChange={(events)=> setGender(events.target.value)}>
                        <option value='Male'>Male</option>
                        <option value='Female'>Female</option>
                      </select>
                    </div>
                  </div>
                </div>  
                
                <br/>
                <div className='field'>
                  <button type='submit' className='button is-success'>Save</button>
                </div>  
            </form>
      </div>
    </div>
  )
}
