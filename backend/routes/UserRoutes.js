const express = require('express');
const {getUsers, getUsersById, createUser, updateUser, deleteUser} = require('../controllers/UserController')

const router = express.Router();

// middleware
// router.use((req, res, next) => {
//   const date = new Date()
//   console.log("Time : ", date.toLocaleDateString())
//   next()
// })

// routes/endpoint
router.get('/users',getUsers);
router.get('/users/:id',getUsersById)
router.post('/users',createUser)
router.put('/users/:id',updateUser)
router.delete('/users/:id',deleteUser)

module.exports =  router;