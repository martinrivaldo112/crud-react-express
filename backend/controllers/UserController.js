const User = require('../models/User');


// get all
exports.getUsers = async (req, res) => {
  try {
    const response = await User.findAll();
    res.status(200).json(response);
  } catch (error) {
    console.log(error.message);
  }
}

// get by id
exports.getUsersById = async (req, res) => {
  try {
    const response = await User.findOne({
      where:{
        id: req.params.id
      }
    });
    res.status(200).json(response);
  } catch (error) {
    console.log(error.message);
  }
}

// create user
exports.createUser = async (req, res) => {
  try {
    const response = await User.create(req.body)
    res.status(201).json({"message": "user berhasil dibuat"});
  } catch (error) {
    console.log(error.message);
  }
}


// update
exports.updateUser = async (req, res) => {
  try {
    const response = await User.update(req.body,{
      where: {
        id: req.params.id
      }
    })
    res.status(201).json({"message": "user berhasil di update"});
  } catch (error) {
    console.log(error.message);
  }
}

exports.deleteUser = async (req, res) => {
  try {
    await User.destroy({
      where:{
        id: req.params.id
      }
    })
    res.status(204).json({message : "user telah di hapus"})
  } catch (error) {
    console.log(error)
  }
}

