const express = require ('express');
const cors =  require('cors');
const UserRoute = require('./routes/UserRoutes')

const app = express();
app.use(cors());
app.use(express.json());

// endpoint
app.use(UserRoute);
// app.use(UserRoute);

app.listen(5000, () => {
  console.log('🎈🎆server berjalan pada port 5000🎄🎄🎄');
})
